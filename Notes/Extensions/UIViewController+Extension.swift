//
//  UIViewController+Extension.swift
//  Notes
//
//  Created by Yaroslav Zarechnyy on 4/22/19.
//  Copyright © 2019 Yaroslav Zarechnyy. All rights reserved.
//

import UIKit
extension UIViewController {
    func showAlertWithMessage(_ message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .cancel) { (alertAction) in
        }
        alert.addAction(action)
        self.present(alert,animated: true, completion: nil)
    } 
}
