//
//  NoteModel.swift
//  Notes
//
//  Created by Yaroslav Zarechnyy on 4/21/19.
//  Copyright © 2019 Yaroslav Zarechnyy. All rights reserved.
//

import Foundation
import CoreData

struct NoteModel {
    var text: String
    var dateOfPosting: Date
    var id: NSManagedObjectID?
    
    init(text: String, dateOfPosting: Date){
        self.text = text
        self.dateOfPosting = dateOfPosting
    }
}
