//
//  ViewController.swift
//  Notes
//
//  Created by Yaroslav Zarechnyy on 4/20/19.
//  Copyright © 2019 Yaroslav Zarechnyy. All rights reserved.
//

import UIKit
import CoreData

class NotesListController: UIViewController {
    
    @IBOutlet weak var noteListTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private let storyboardName = "Main"
    private let addNewNoteViewControllerID = "addNewNoteController"
    private let noteEntity = "Note"

    private var isSearching = false
    private var searchNote = [NoteModel]()
    private var filteredData = [NoteModel]()
    
    private var noteList = [NoteModel](){
        didSet{
            DispatchQueue.main.async {
                self.noteListTableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.noteList.removeAll()
        self.fetchResults()
    }

    @IBAction func didActionAddNewNote(_ sender: UIBarButtonItem) {
        let addNoteVC = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: self.addNewNoteViewControllerID) as! AddNewNoteController
        addNoteVC.callbackFotNote = { note in
            self.noteList.insert(note, at: 0)
        }
        self.navigationController?.pushViewController(addNoteVC, animated: true)
    }
    
    @IBAction func didActionSortNotes(_ sender: UIBarButtonItem) {
        presentAlert()
    }
    
    private func sortByNew() {
        let message = NSLocalizedString("Error. Your notes list is empty.", comment: "")
        self.noteList.isEmpty ? showAlertWithMessage(message) : self.noteList.sort(by: { $0.dateOfPosting.isLessThanDate(dateToCompare: $1.dateOfPosting)})
    }
    
    private func sortByOld() {
        let message = NSLocalizedString("Error. Your notes list is empty.", comment: "")
        self.noteList.isEmpty ? showAlertWithMessage(message) : self.noteList.sort(by: { $1.dateOfPosting.isLessThanDate(dateToCompare: $0.dateOfPosting)})
    }
    
    
    private func addNoteVC(note: NoteModel) -> AddNewNoteController {
       let addNoteVC = UIStoryboard(name: self.storyboardName, bundle: nil).instantiateViewController(withIdentifier: self.addNewNoteViewControllerID) as! AddNewNoteController
        addNoteVC.note = note
        return addNoteVC
    }
    
    //MARK:-Core Data methods
    private func fetchResults(){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext =
            appDelegate.persistentContainer.viewContext
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: self.noteEntity)
        do {
            let fetchedResult = try managedContext.fetch(fetchRequest)
            if fetchedResult.count != 0 {
                for i in 0...(fetchedResult.count-1) {
                    let entityModel = fetchedResult[i] as! Note
                    var note = NoteModel(text: entityModel.text!, dateOfPosting: entityModel.date!)
                    note.id = entityModel.objectID
                    self.noteList.append(note)
                }
            }
            self.noteList.reverse()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    private func deleteResult(_ note: NoteModel) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: self.noteEntity)
        do {
            let fetchedResult = try managedContext.fetch(fetchRequest)
            for i in 0...(fetchedResult.count-1) {
                let entityModel = fetchedResult[i]
                if entityModel.objectID == note.id {
                    managedContext.delete(entityModel)
                    do {
                        try managedContext.save()
                    } catch let error{
                        print(error)
                    }
                }
            }
        } catch let error as NSError {
            print("Could not delete. \(error), \(error.userInfo)")
        }
    }
    
    fileprivate func configure() {
        self.noteListTableView.delegate = self
        self.noteListTableView.dataSource = self
        self.noteListTableView.tableFooterView = UIView()
        searchBar.delegate = self
    }
    
}


//MARK:-UITableViewDelegate & UITableViewDataSource
extension NotesListController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.isSearching ? self.searchNote : self.noteList).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "noteCell", for: indexPath) as! NoteCell
        self.isSearching ? cell.setupWithModel(model: self.searchNote[indexPath.row]) : cell.setupWithModel(model: self.noteList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if isSearching {
            let vc = addNoteVC(note: self.searchNote[indexPath.row])
            vc.callbackFotNote = { note in
                self.noteList[indexPath.row] = note
                self.searchNote[indexPath.row] = note
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = addNoteVC(note: self.noteList[indexPath.row])
            vc.callbackFotNote = { note in
                self.noteList[indexPath.row] = note
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.deleteResult(noteList[indexPath.row])
            self.noteList.remove(at: indexPath.row)
            self.noteListTableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
}

//MARK:-UISearchBarDelegate
extension NotesListController: UISearchBarDelegate {
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.isSearching = false
        DispatchQueue.main.async {
            self.noteListTableView.reloadData()
        }
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.isSearching = (searchText != "")
        guard self.isSearching else {
            self.searchNote = [NoteModel]()
            self.noteListTableView.reloadData()
            return
        }
        self.searchNote = self.noteList.filter({ (note) -> Bool in
            return note.text.lowercased().contains(searchText.lowercased())
        })
        self.noteListTableView.reloadData()
    }
    
}

//MARK:-AlertPresentable
extension NotesListController: AlertPresentable {
    
    var alertComponents: AlertComponents {
        var message = NSLocalizedString("From older to newer", comment: "")
        let didActionCreateNewUser = AlertActionComponent(title: message, handler: {_ in
            self.sortByNew()
        })
        message = NSLocalizedString("From newer to older", comment: "")
        let didActionAddFromPreviousGame = AlertActionComponent(title: message, handler: { _ in
            self.sortByOld()
        })
        message = NSLocalizedString("Cancel", comment: "")
        let didActionCloseAlert = AlertActionComponent(title: message, style: .cancel, handler: nil)
        let alertComponents = AlertComponents(title: nil, message: nil, actions: [didActionCreateNewUser, didActionAddFromPreviousGame, didActionCloseAlert], completion: {
        })
        return alertComponents

    }
    
}
