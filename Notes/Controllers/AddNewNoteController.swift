//
//  AddNewNoteController.swift
//  Notes
//
//  Created by Yaroslav Zarechnyy on 4/20/19.
//  Copyright © 2019 Yaroslav Zarechnyy. All rights reserved.
//

import UIKit
import CoreData

class AddNewNoteController: UIViewController {
    
    @IBOutlet var addNewNoteView: AddNewNoteView!
    @IBOutlet weak var noteTextView: UITextView!
    
    private let noteEntity = "Note"
    private var results: [NSManagedObject] = []
    var callbackFotNote: ((NoteModel)->())?
    var note: NoteModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
    }
    
    fileprivate func configure() {
        if note?.text == nil {
            let title = NSLocalizedString("Save", comment: "")
            let saveButton = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(saveNotes))
            self.navigationItem.rightBarButtonItem = saveButton
        }else {
            let shareButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareNote))
            let editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editNote))
            self.navigationItem.rightBarButtonItems = [editButton,shareButton]
            noteTextView.text = note?.text
        }
    }
    
    @objc func saveNotes() {
        if !noteTextView.text.isEmpty {
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM HH:mm"            
            let noteModel = NoteModel(text: noteTextView.text, dateOfPosting: date)
            callbackFotNote?(noteModel)
            self.save(noteModel)
            self.navigationController?.popViewController(animated: true)
        }else {
            let message = NSLocalizedString("Error. Your note is empty.", comment: "")
            showAlertWithMessage(message)
        }
    }
    
    @objc func editNote() {
        if !noteTextView.text.isEmpty {
            note?.text = noteTextView.text
            self.updateNote(note!)
            self.navigationController?.popViewController(animated: true)
        }else {
            let message = NSLocalizedString("Error. Your note is empty.", comment: "")
            showAlertWithMessage(message)
        }
    }
    
    @objc func shareNote() {
        let activity = UIActivityViewController(
            activityItems: [note?.text ?? ""],
            applicationActivities: nil
        )        
        present(activity, animated: true, completion: nil)
    }
    
    //MARK-Core Data methods
    private func save(_ note:NoteModel) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity =
            NSEntityDescription.entity(forEntityName: self.noteEntity,
                                       in: managedContext)!
        let result = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        result.setValue(note.text, forKey: "text")
        result.setValue(note.dateOfPosting, forKey: "date")
        do{
            try managedContext.save()
            results.append(result)
        }catch let error as NSError{
            print(error)
        }
    }
    
    private func updateNote(_ note:NoteModel) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: self.noteEntity)
        do {
            let fetchedResult = try managedContext.fetch(fetchRequest)
            for i in 0...(fetchedResult.count-1) {
                let entityModel = fetchedResult[i]
                if entityModel.objectID == note.id {
                    entityModel.setValue(note.text, forKey: "text")
                    do {
                        try managedContext.save()
                        self.callbackFotNote?(note)
                    } catch let error{
                        print(error)
                    }
                }
            }
        } catch let error as NSError {
            print("Could not update. \(error), \(error.userInfo)")
        }
    }
    
}
