//
//  NoteCell.swift
//  Notes
//
//  Created by Yaroslav Zarechnyy on 4/21/19.
//  Copyright © 2019 Yaroslav Zarechnyy. All rights reserved.
//

import UIKit

class NoteCell: UITableViewCell {
        
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var dateOfNoteLabel: UILabel!
    @IBOutlet weak var timeOfNoteLabel: UILabel!
    
    private let formatter = DateFormatter()
    
    func setupWithModel(model: NoteModel) {
        if model.text.count < 100{
            noteLabel.text = model.text
        }else {
            noteLabel.text = "\(model.text.prefix(100))..."
        }
        formatter.dateFormat = "dd.MM"
        dateOfNoteLabel.text = formatter.string(from: model.dateOfPosting)
        formatter.dateFormat = "HH:mm"
        timeOfNoteLabel.text = formatter.string(from: model.dateOfPosting)
        accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
    }
    
}
